import cv2
import numpy as np

# creates a matrix filled with 0
# can be used to create an 512 per 512 pixel image with 0 color
# which is "black", np.uint8 introduce 8bit colors
img = np.zeros((512, 512, 3), np.uint8)

# changing color to blue
img[:] = 255,0,0

cv2.imshow("Black Image", img)

cv2.waitKey(10000)