# https://www.youtube.com/watch?v=WQeoO7MI0Bs

import cv2
import numpy as np
import sys

# photo part
img = cv2.imread("Resources/lena.png")
# kernel is to set metrics for the edge thickness 
kernel = np.ones((5,5), np.uint8)

imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
imgBlur = cv2.GaussianBlur(imgGray, (7, 7), 0)

# edge detection
# two threshold to level edge detecttion
imgCanny = cv2.Canny(img, 150, 200)

# dilation for increase edge thickness
# used with the imgCanny edges image 
# more iterations means larger edges
imgDialation = cv2.dilate(imgCanny, kernel, iterations = 1)

# erosion allows for thickier edges, does not work with edges, will erase them
imgEroded = cv2.erode(imgDialation, kernel, iterations = 1)

cv2.imshow("Gray image", imgGray)
cv2.imshow("Blur image", imgBlur)
cv2.imshow("Canny Image", imgCanny)
cv2.imshow("Dialated Image", imgDialation)
cv2.imshow("Eroded Image", imgEroded)

# infinite delay 0 ms, 1000 means 1s
cv2.waitKey(10000)

# resize cv2 coordinates system is as in pillow library not the usual one
img = cv2.imread("Resources/lambo.jpg")
# get height and width in pixel, 3 is the number of tunnel (3 colors?)
# beware resize dimensions
imgResize = cv2.resize(img,(900,600))
cv2.imshow("Resized image", imgResize)

# cropping image
imgCropped = img[0:200, 200:500]
cv2.imshow("Cropped image", imgCropped)

# camera capture
cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)
cap.set(10, 50)

while True:
    success, img = cap.read()
    cv2.imshow("Video", img)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break